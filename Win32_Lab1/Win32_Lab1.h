#pragma once

#include "resource.h"
#include "windows.h"


void ClearWindow(HWND hWnd);
LONG GetWidthWindow(HWND hWnd);
void LoadWindowSelect(WNDCLASSEX &wc, HINSTANCE hInstar);
LONG GetLeftBorderWindow(HWND hWnd);
void CheckOrientMovie(HWND hWnd, TypeMovie &type);
LONG GetTopBorderWindow(HWND hWnd);
void DrawImage(HWND hWnd, const wchar_t *image);
