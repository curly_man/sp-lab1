#include <windows.h>
#define ImageMoveNext L"picture.bmp"

HWND btClear;
HBITMAP hBitmap;

//��������� ��� �������� 
int widthPicture = 600;

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

void LoadWindowSelect(WNDCLASSEX &wc, HINSTANCE hInstance)
{
	wc = { NULL };
	wc.cbSize = sizeof(WNDCLASSEX); //������ ��� �����
	wc.style = CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc = WndProc; //�������, ������� ����� ������������ ���������
	wc.hInstance = hInstance; //����������� ����
	wc.cbWndExtra = 0;
	wc.cbClsExtra = 0;
	wc.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH); //����
	wc.lpszClassName = TEXT("MainWindow"); //
}

void DrawImage(HWND hWnd, const wchar_t *image)
{
	HDC hDC, hMemDC;
	BITMAP bm;
	RECT rect = { NULL };
	GetWindowRect(hWnd, &rect);
	float k = 0.6667;
	hDC = GetDC(hWnd);
	hMemDC = CreateCompatibleDC(hDC);
	widthPicture = rect.right - rect.left;
	if (widthPicture*k > rect.bottom - rect.top) {
		widthPicture = (rect.bottom - rect.top) / k;
	}
	hBitmap = (HBITMAP)LoadImage(NULL, image, IMAGE_BITMAP, widthPicture, (int)widthPicture*k, LR_LOADFROMFILE); //�������� ��������
	GetObject(hBitmap, sizeof(BITMAP), &bm);//�������� � ��� �����
	SelectObject(hMemDC, hBitmap);//���� ������
	BitBlt(hDC, (rect.right - rect.left) / 2 - bm.bmWidth / 2, ((rect.bottom - rect.top) / 2) - bm.bmHeight / 2, widthPicture, (int)widthPicture*k, hMemDC, 0, 0, SRCCOPY);
	DeleteDC(hMemDC);//��� ����������� ��� ������� �� �������
	ReleaseDC(hWnd, hDC);
	DeleteObject(hBitmap);
}

int WINAPI WinMain(HINSTANCE hInstance,
	HINSTANCE hPrevInstance,
	LPSTR     lpCmdLine,
	int       nCmdShow)
{
	// ������� ���������� ����
	WNDCLASSEX wc; // ������ ���������, ��� ��������� � ������ ������ WNDCLASSEX
	LoadWindowSelect(wc, hInstance);
	if (!RegisterClassEx(&wc))
		return -1;
	HWND hWnd = CreateWindow(TEXT("MainWindow"), TEXT("Lab 1"), WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, CW_USEDEFAULT, 1000, 700, 0, 0, hInstance, 0);
	ShowWindow(hWnd, nCmdShow);//�������� ����

	UpdateWindow(hWnd);//��������������
	MSG message = { 0 };
	while (GetMessage(&message, 0, 0, 0))//���� ��������� ��������� 
	{
		TranslateMessage(&message);
		DispatchMessage(&message);
	}
	return  0;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {

	HDC hDeviceContext;
	PAINTSTRUCT paintStruct;
	RECT rectPlace;
	HFONT hFont;

	static PTCHAR text;
	static int size = 0;
	//����� ��� ���������

	switch (message)
	{
	case WM_CREATE:
	{
		DrawImage(hWnd, ImageMoveNext); //������
		UpdateWindow(hWnd);
	}
	case WM_SIZE: //������
	{					   
		DrawImage(hWnd, ImageMoveNext); //������
		UpdateWindow(hWnd);
		break;
	}
	case WM_DESTROY:
	{
		PostQuitMessage(WM_QUIT); //����� �� ���������
		break;
	}
	}
	return DefWindowProc(hWnd, message, wParam, lParam);
}